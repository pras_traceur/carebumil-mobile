part of 'pages.dart';

class AkunPage extends StatelessWidget {
  final LocalStorage storage = new LocalStorage('carebumil');
  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 136,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/logo.png"))),
                ),
                Container(
                  margin: EdgeInsets.only(top: 70, bottom: 16),
                  child: Text(
                    "Halo Ibu " + storage.getItem("nama"),
                    style: blackTextFont.copyWith(fontSize: 20),
                  ),
                ),
                Text(
                  "(Sistem Deteksi Depresi Antenatal)",
                  style: greyTextFont.copyWith(
                      fontSize: 16, fontWeight: FontWeight.w300),
                ),
                Text(
                  "Versi 1.0 Copyright 2020",
                  style: greyTextFont.copyWith(
                      fontSize: 16, fontWeight: FontWeight.w300),
                ),
                Container(
                  width: 250,
                  height: 46,
                  margin: EdgeInsets.only(top: 70, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "LOG OUT",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        storage.setItem('login', '0');
                        storage.setItem('id', '');
                        storage.setItem('email', '');
                        storage.setItem('password', '');
                        storage.setItem('nama', '');
                        storage.setItem('page', '0');
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return LoginPage();
                        }));
                      }),
                ),
              ],
            )));
  }
}
