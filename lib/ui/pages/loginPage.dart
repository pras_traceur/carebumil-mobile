part of 'pages.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  Pengguna result = null;
  String success = null;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isSigninValid = false;

  @override
  Widget build(BuildContext context) {
    void saveStorage(id, email, password, nama) {
      storage.setItem('id', id);
      storage.setItem('email', email);
      storage.setItem('password', password);
      storage.setItem('nama', nama);
    }

    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 60,
                  ),
                  SizedBox(
                    height: 70,
                    child: Image.asset("assets/logo.png"),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 70, bottom: 40),
                    child: Text(
                      "Welcome Back Mom,\nExplorer!",
                      style: blackTextFont.copyWith(fontSize: 20),
                    ),
                  ),
                  TextField(
                    onChanged: (text) {
                      setState(() {
                        isEmailValid = EmailValidator.validate(text);
                      });
                    },
                    controller: emailController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Email Pengguna",
                        hintText: "Email Pengguna"),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    onChanged: (text) {
                      setState(() {
                        isPasswordValid = text.length >= 6;
                      });
                    },
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Password",
                        hintText: "Password"),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Center(
                    child: Container(
                      width: 50,
                      height: 50,
                      margin: EdgeInsets.only(top: 40, bottom: 30),
                      child: isSigninValid
                          ? SpinKitFadingCircle(
                              color: mainColor,
                            )
                          : FloatingActionButton(
                              elevation: 0,
                              child: Icon(
                                Icons.arrow_forward,
                                color: isEmailValid && isPasswordValid
                                    ? Colors.white
                                    : Color(0xFFBEBEBE),
                              ),
                              backgroundColor: isEmailValid && isPasswordValid
                                  ? mainColor
                                  : Color(0xFFE4E4E4),
                              onPressed: isEmailValid && isPasswordValid
                                  ? () async {
                                      setState(() {
                                        isSigninValid = true;
                                      });

                                      await Pengguna.loginApi(
                                              emailController.text,
                                              passwordController.text)
                                          .then((value) {
                                        result = value;
                                        if (result.success == 'ya' &&
                                            result.idperanpengguna == "3") {
                                          storage.setItem('login', '1');
                                          saveStorage(
                                              result.idpengguna,
                                              result.emailpengguna,
                                              result.password,
                                              result.namapengguna);
                                          Flushbar(
                                            duration: Duration(seconds: 4),
                                            flushbarPosition:
                                                FlushbarPosition.TOP,
                                            backgroundColor: greenColor,
                                            message: "Selamat Datang Bu " +
                                                result.namapengguna,
                                          )..show(context);
                                          Navigator.pushReplacement(context,
                                              MaterialPageRoute(
                                                  builder: (context) {
                                            return Wrapper();
                                          }));
                                        } else {
                                          storage.setItem('login', '0');
                                          Flushbar(
                                            duration: Duration(seconds: 4),
                                            flushbarPosition:
                                                FlushbarPosition.TOP,
                                            backgroundColor: mainColor,
                                            message:
                                                "Login Gagal, Pastikan Email/Password Benar !",
                                          )..show(context);
                                        }
                                      });
                                      setState(() {
                                        isSigninValid = false;
                                      });
                                    }
                                  : null,
                            ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Daftar Akun Baru? ",
                        style:
                            greyTextFont.copyWith(fontWeight: FontWeight.w400),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return SignupPage();
                          }));
                        },
                        child: Text(
                          "Sign Up",
                          style: mainTextFont,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
