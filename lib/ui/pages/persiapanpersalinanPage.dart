part of 'pages.dart';

class PersiapanpersalinanPage extends StatefulWidget {
  @override
  _PersiapanpersalinanPageState createState() =>
      _PersiapanpersalinanPageState();
}

class _PersiapanpersalinanPageState extends State<PersiapanpersalinanPage> {
  final LocalStorage storage = new LocalStorage('carebumil');

  TextEditingController rencanaController = TextEditingController();
  TextEditingController tempatController = TextEditingController();

  int _rgProgramming1 = -1;
  int _rgProgramming2 = -1;
  int _rgProgramming3 = 1;
  int _rgProgramming4 = 1;

  bool status = true;
  String nomor = "";
  String coment = "";
  String _selectedValue;
  PersiapanPersalinan result;
  int count = 0;

  bool isRencana = false;
  bool isTempat = false;

  final List<RadioGroup> _kelas = [
    RadioGroup(index: 1, text: "Ya"),
    RadioGroup(index: 0, text: "Tidak"),
  ];

  final List<RadioGroup> _keinginan = [
    RadioGroup(index: 1, text: "Normal"),
    RadioGroup(index: 0, text: "Caesar"),
  ];

  final List<RadioGroup> _asuransi = [
    RadioGroup(index: 1, text: "Ada"),
    RadioGroup(index: 0, text: "Belum Ada"),
  ];

  final List<RadioGroup> _dana = [
    RadioGroup(index: 1, text: "Ada"),
    RadioGroup(index: 0, text: "Belum Ada"),
  ];

  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    if (count == 0) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        PersiapanPersalinan.getPersiapanPersalinan(storage.getItem("id"))
            .then((value) {
          result = value;
          if (result.success == 'ya') {
            setState(() {
              rencanaController.text = result.rencanapersalinan;
              tempatController.text = result.di;

              if (result.kelashamil == "ya") {
                _rgProgramming1 = 1;
              } else {
                _rgProgramming1 = 0;
              }

              if (result.keinginanbersalin == "normal") {
                _rgProgramming2 = 1;
              } else {
                _rgProgramming2 = 0;
              }

              if (result.asuransi == "ada") {
                _rgProgramming3 = 1;
              } else {
                _rgProgramming3 = 0;
              }

              if (result.persiapandana == "ada") {
                _rgProgramming4 = 1;
              } else {
                _rgProgramming4 = 0;
              }
            });
          }
        });

        count++;
      });
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Persiapan Persalinan"),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: ListView(
              children: <Widget>[
                Text(
                  "1. Selama hamil, pernahkah mengikuti kelas hamil ?",
                  textAlign: TextAlign.left,
                ),
                _buildRadioButton(1),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "2. Rencana persalinan ingin ditolong :",
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  onChanged: (text) {
                    setState(() {
                      isRencana = text.length >= 1;
                    });
                  },
                  controller: rencanaController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: "Wajib diisi...",
                      hintText: "Dokter/Bidan/Lainnya, Sebutkan"),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "3. Rencana tempat persalinan :",
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  onChanged: (text) {
                    setState(() {
                      isTempat = text.length >= 1;
                    });
                  },
                  controller: tempatController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: "Wajib diisi...",
                      hintText: "Rumah Sakit/Klinik Bidan/Lainnya, Sebutkan"),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text("4. Keinginan bersalin secara"),
                _buildRadioButton(2),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming3),
                Text("5. Apakah ada asuransi untuk pembiayaan persalinan ?"),
                _buildRadioButton(3),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming4),
                Text("6. Apakah sudah ada persiapan dana untuk persalinan ?"),
                _buildRadioButton(4),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "Simpan",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        status = true;

                        if (_rgProgramming1 == -1) {
                          status = false;
                          nomor = "1";
                        }

                        if (rencanaController.text.length < 1) {
                          status = false;
                          nomor = "2";
                        }

                        if (tempatController.text.length < 1) {
                          status = false;
                          nomor = "3";
                        }

                        if (_rgProgramming2 == -1) {
                          status = false;
                          nomor = "4";
                        }
                        if (_rgProgramming3 == -1) {
                          status = false;
                          nomor = "5";
                        }
                        if (_rgProgramming4 == -1) {
                          status = false;
                          nomor = "6";
                        }

                        if (status == false) {
                          Flushbar(
                            duration: Duration(seconds: 4),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: mainColor,
                            message: "Jawab Soal Nomor " +
                                nomor +
                                " Lebih dahulu !!!",
                          )..show(context);
                        } else {
                          PersiapanPersalinan.apiSubmitPersiapanPersalinan(
                                  storage.getItem("id"),
                                  _rgProgramming1.toString(),
                                  rencanaController.text,
                                  tempatController.text,
                                  _rgProgramming2.toString(),
                                  _rgProgramming3.toString(),
                                  _rgProgramming4.toString())
                              .then((value) => {
                                    if (value.success == "ya")
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: greenColor,
                                          message: "Simpan Data Berhasil",
                                        )..show(context)
                                      }
                                    else
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: mainColor,
                                          message:
                                              "Simpan Data Gagal, Coba Lagi !",
                                        )..show(context)
                                      }
                                  });
                        }
                      }),
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: RaisedButton(
                      child: Text(
                        "Kembali",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: colorPurple,
                      onPressed: () {
                        storage.setItem("page", 0);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage();
                        }));
                      }),
                )
              ],
            ),
          ),
        ));
  }

  Widget _alertIcon(parameter) {
    if (parameter == 0) {
      return Icon(
        MdiIcons.alert,
        color: Colors.red,
      );
    } else {
      return Container();
    }
  }

  // widget radio
  Widget _buildRadioButton(parameter) {
    if (parameter == 1) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _kelas
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming1,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming1 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 2) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _keinginan
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming2,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming2 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 3) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _asuransi
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming3,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming3 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 4) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _dana
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming4,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming4 = value;
                    });
                  },
                ))
            .toList(),
      );
    }
  }
}
