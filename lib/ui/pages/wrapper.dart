part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LocalStorage storage = new LocalStorage('carebumil');
    if (storage.getItem('login') == '1') {
      return HomePage();
    } else if (storage.getItem('login') == '0') {
      return LoginPage();
    } else {
      return SplashPage();
    }
  }
}
