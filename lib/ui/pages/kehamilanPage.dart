part of 'pages.dart';

class KehamilanPage extends StatefulWidget {
  @override
  _KehamilanPageState createState() => _KehamilanPageState();
}

class _KehamilanPageState extends State<KehamilanPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  TextEditingController hamilkeController = TextEditingController();
  TextEditingController kalauadaController = TextEditingController();
  TextEditingController jikaController = TextEditingController();

  String haid;
  String hpl;

  int _rgProgramming3 = 0;
  int _rgProgramming4 = 0;
  int _rgProgramming6 = 0;
  int _rgProgramming7 = 0;

  bool status = true;
  String nomor = "";
  String coment = "";
  String _selectedValue;
  Kehamilan result;
  int count = 0;

  int isHamilke = 0;

  bool isHamil = false;
  bool isKalau = false;
  bool isJika = false;

  final List<RadioGroup> _yesno = [
    RadioGroup(index: 1, text: "Ya"),
    RadioGroup(index: 0, text: "Tidak"),
  ];

  final List<RadioGroup> _isno = [
    RadioGroup(index: 1, text: "Ada"),
    RadioGroup(index: 0, text: "Tidak"),
  ];

  DateTime _dateTime;
  DateFormat formatter = DateFormat('yyyy-MM-dd');
  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    if (count == 0) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        Kehamilan.getKehamilan(storage.getItem("id")).then((value) {
          result = value;
          if (result.success == 'ya') {
            setState(() {
              hamilkeController.text = result.hamilke;
              isHamilke = int.parse(result.hamilke);
              if (result.hamildirencanakan == "ya") {
                _rgProgramming3 = 1;
              } else {
                _rgProgramming3 = 0;
              }
              if (result.harapanjeniskelamin == "ada") {
                _rgProgramming4 = 1;
              } else {
                _rgProgramming4 = 0;
              }
              kalauadaController.text = result.kalauada;
              if (result.antidepresi == "ya") {
                _rgProgramming6 = 1;
              } else {
                _rgProgramming6 = 0;
              }
              if (result.keluhan == "ya") {
                _rgProgramming7 = 1;
              } else {
                _rgProgramming7 = 0;
              }
              jikaController.text = result.jikaada;
              haid = result.tanggalpertamahaid;
              hpl = result.hpl;
            });
          }
        });

        count++;
      });
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Kehamilan"),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: ListView(
              children: <Widget>[
                _alertIcon(isHamilke, 0),
                Text(
                  "1. Hamil ke ?",
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  onChanged: (text) {
                    setState(() {
                      isHamil = text.length >= 1;
                    });
                  },
                  controller: hamilkeController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: "Wajib diisi...",
                      hintText: "isi Kehamilan ke-berapa"),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(haid == null
                    ? "2. Hari Pertama Haid Terakhir : Belum Memilih Hari"
                    : "2. Hari Pertama Haid Terakhir : " + haid),
                Text("     Hari Perkiraan Lahir : " + hpl.toString()),
                RaisedButton(
                    child: Text(
                      "Ubah Tanggal Haid Terakhir",
                      style: whiteTextFont.copyWith(fontSize: 16),
                    ),
                    color: colorPurple,
                    onPressed: () {
                      showDatePicker(
                              context: context,
                              initialDate: _dateTime == null
                                  ? DateTime.now()
                                  : _dateTime,
                              firstDate: DateTime(2001),
                              lastDate: DateTime(2031))
                          .then((date) {
                        setState(() {
                          _dateTime = date;
                          haid = formatter.format(date);
                        });
                      });
                    }),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming3, 1),
                Text("3. Apakah hamil ini direncanakan ?"),
                _buildRadioButton(3),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming4, 0),
                Text("4. Harapan keluarga untuk jenis kelamin bayi"),
                _buildRadioButton(4),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "5. Kalau ada:",
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  onChanged: (text) {
                    setState(() {
                      isKalau = text.length >= 1;
                    });
                  },
                  controller: kalauadaController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: "Wajib diisi...",
                      hintText: "Laki-laki/Perempuan"),
                ),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming6, 0),
                Text(
                    "6. Apakah sedang mengkonsumsi obat- obatan antidepresi ?"),
                _buildRadioButton(6),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming7, 0),
                Text("7. Apakah selama hamil ini mengalami keluhan/masalah ?"),
                _buildRadioButton(7),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "8. Jika Ya Sebutkan:",
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  onChanged: (text) {
                    setState(() {
                      isJika = text.length >= 1;
                    });
                  },
                  controller: jikaController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: "Wajib diisi...",
                      hintText: "Isi Keluhan/Masalah"),
                ),
                SizedBox(
                  height: 20.0,
                ), //
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "Simpan",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        status = true;

                        if (hamilkeController.text.length < 1) {
                          status = false;
                          nomor = "1";
                        }
                        if (haid == null) {
                          status = false;
                          nomor = "2";
                        }
                        if (_rgProgramming3 == -1) {
                          status = false;
                          nomor = "3";
                        }
                        if (_rgProgramming4 == -1) {
                          status = false;
                          nomor = "4";
                        }
                        if (kalauadaController.text.length < 1) {
                          status = false;
                          nomor = "5";
                        }
                        if (_rgProgramming6 == -1) {
                          status = false;
                          nomor = "6";
                        }
                        if (_rgProgramming7 == -1) {
                          status = false;
                          nomor = "7";
                        }
                        if (jikaController.text.length < 1) {
                          status = false;
                          nomor = "8";
                        }

                        if (status == false) {
                          Flushbar(
                            duration: Duration(seconds: 4),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: mainColor,
                            message: "Jawab Soal Nomor " +
                                nomor +
                                " Lebih dahulu !!!",
                          )..show(context);
                        } else {
                          Kehamilan.apiSubmitKehamilan(
                                  storage.getItem("id"),
                                  hamilkeController.text,
                                  haid,
                                  _rgProgramming3.toString(),
                                  _rgProgramming4.toString(),
                                  kalauadaController.text,
                                  _rgProgramming6.toString(),
                                  _rgProgramming7.toString(),
                                  jikaController.text)
                              .then((value) => {
                                    if (value.success == "ya")
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: greenColor,
                                          message: "Simpan Data Berhasil",
                                        )..show(context)
                                      }
                                    else
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: mainColor,
                                          message:
                                              "Simpan Data Gagal, Coba Lagi !",
                                        )..show(context)
                                      }
                                  });
                        }
                      }),
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: RaisedButton(
                      child: Text(
                        "Kembali",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: colorPurple,
                      onPressed: () {
                        storage.setItem("page", 0);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage();
                        }));
                      }),
                )
              ],
            ),
          ),
        ));
  }

  Widget _alertIcon(parameter, rencana) {
    if (rencana == 0) {
      if (parameter == 1) {
        return Icon(
          MdiIcons.alert,
          color: Colors.red,
        );
      } else {
        return Container();
      }
    } else {
      if (parameter == 0) {
        return Icon(
          MdiIcons.alert,
          color: Colors.red,
        );
      } else {
        return Container();
      }
    }
  }

  // widget radio
  Widget _buildRadioButton(parameter) {
    if (parameter == 3) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _yesno
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming3,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming3 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 4) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _isno
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming4,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming4 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 6) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _yesno
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming6,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming6 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 7) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _yesno
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming7,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming7 = value;
                    });
                  },
                ))
            .toList(),
      );
    }
  }
}
