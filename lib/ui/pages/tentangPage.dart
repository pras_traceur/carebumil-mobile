part of 'pages.dart';

class TentangPage extends StatefulWidget {
  @override
  _TentangPageState createState() => _TentangPageState();
}

class _TentangPageState extends State<TentangPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    return Stack(children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 20),
        alignment: Alignment.topCenter,
        child: Text(
          "Tentang Saya",
          style: blackTextFont.copyWith(fontSize: 20),
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 10, right: 10, top: 55),
        child: GridView.count(
          crossAxisCount: 2,
          mainAxisSpacing: 1,
          crossAxisSpacing: 1,
          primary: false,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return DatadiriPage();
                }));
              },
              child: SizedBox(
                height: 50,
                child: Image.asset("assets/datadiri.png"),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return KehamilanPage();
                }));
              },
              child: SizedBox(
                height: 50,
                child: Image.asset("assets/kehamilanini.png"),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return RiwayatobstetriPage();
                }));
              },
              child: SizedBox(
                height: 50,
                child: Image.asset("assets/riwayatobstetri.png"),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return DukungansosialPage();
                }));
              },
              child: SizedBox(
                height: 50,
                child: Image.asset("assets/dukungansosial.png"),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return PersiapanpersalinanPage();
                }));
              },
              child: SizedBox(
                height: 50,
                child: Image.asset("assets/persiapanpersalinan.png"),
              ),
            ),
            GestureDetector(
              onTap: () {
                storage.setItem("page", 4);
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return HomePage();
                }));
              },
              child: SizedBox(
                height: 50,
                child: Image.asset("assets/carebumil.png"),
              ),
            ),
          ],
        ),
      )
    ]);
  }
}
