part of 'pages.dart';

class HasiltestPage extends StatefulWidget {
  @override
  _HasiltestPageState createState() => _HasiltestPageState();
}

class _HasiltestPageState extends State<HasiltestPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  Test result;
  String tanggal;
  String score;
  String status = null;
  String hasil;
  double persen;
  double total;
  int angka;
  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    Test.getLastTest(storage.getItem('id')).then((value) {
      result = value;
      if (result.success == 'ya') {
        setState(() {
          tanggal = result.tanggal;
          score = result.score;
          hasil = result.hasil;
          total = double.parse(score) / (30 / 100);
          persen = total / 100;
          if (result.hasil == "Berisiko mengalami depresi") {
            status =
                'Silakan lanjutkan ke sesi konsultasi\nlangsung dengan bidan';
          } else {
            status =
                'Silakan isi kuesioner deteksi depresi kembali\ndi trimester selanjutnya';
          }
        });
      }
    });

    if (status == null) {
      return Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 20),
          alignment: Alignment.topCenter,
          child: Text(
            "Hasil Test Terbaru",
            style: blackTextFont.copyWith(fontSize: 20),
          ),
        ),
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage("assets/nodata.png"))),
        )
      ]);
    } else {
      return Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 20),
          alignment: Alignment.topCenter,
          child: Text(
            "Hasil Test Terbaru",
            style: blackTextFont.copyWith(fontSize: 20),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 80, left: 20),
          child: Row(
            children: <Widget>[
              Icon(
                MdiIcons.clockOutline,
              ),
              Text(
                "Tanggal : " + tanggal,
                style: blackTextFont.copyWith(fontSize: 15),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 130),
          alignment: Alignment.topCenter,
          child: Text(
            score + "/30",
            style: GoogleFonts.lato(fontSize: 100, fontWeight: FontWeight.w900),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 250, left: 10, right: 10),
          alignment: Alignment.topCenter,
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: new LinearPercentIndicator(
              width: MediaQuery.of(context).size.width - 50,
              animation: true,
              lineHeight: 20.0,
              animationDuration: 2000,
              percent: persen,
              center: Text(
                total.toInt().toString() + "%",
                style: whiteTextFont.copyWith(
                    fontSize: 16, fontWeight: FontWeight.w300),
              ),
              linearStrokeCap: LinearStrokeCap.roundAll,
              progressColor: Colors.orange,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 300),
          alignment: Alignment.topCenter,
          child: Text(
            hasil + "\n" + status,
            style: blackTextFont.copyWith(fontSize: 14),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 370),
          alignment: Alignment.topCenter,
          child: Text(
            "Skor hasil Tes berdasarkan tes hari ini.\nPerubahan hormonal, fisik, dan psikologis ibu hamil\nmenyebabkan perubahan emosional selama\nkehamilan. Ibu hamil disarankan untuk\nmelakukan tes deteksi depresi antenatal\ntiap trimester",
            style: greyTextFont.copyWith(fontSize: 13),
            textAlign: TextAlign.center,
          ),
        ),
        Center(
          child: Container(
            width: 250,
            height: 46,
            margin: EdgeInsets.only(top: 400, bottom: 10),
            child: RaisedButton(
                child: Text(
                  "Ingatkan Saya Untuk Test Kembali",
                  style: whiteTextFont.copyWith(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return SignupPage();
                  }));
                }),
          ),
        ),
      ]);
    }
  }
}
