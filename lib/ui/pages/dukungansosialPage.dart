part of 'pages.dart';

class DukungansosialPage extends StatefulWidget {
  @override
  _DukungansosialPageState createState() => _DukungansosialPageState();
}

class _DukungansosialPageState extends State<DukungansosialPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  int _rgProgramming0 = -1;
  int _rgProgramming1 = -1;
  int _rgProgramming2 = -1;
  int _rgProgramming3 = -1;
  int _rgProgramming4 = -1;
  int _rgProgramming5 = -1;
  int _rgProgramming6 = -1;

  bool status = true;
  String nomor = "";
  String coment = "";
  String _selectedValue;
  DukunganSosial result;
  int count = 0;

  final List<RadioGroup> _kekerasan = [
    RadioGroup(index: 1, text: "Ya"),
    RadioGroup(index: 0, text: "Tidak"),
  ];

  final List<RadioGroup> _programmingList = [
    RadioGroup(index: 5, text: "5"),
    RadioGroup(index: 4, text: "4"),
    RadioGroup(index: 3, text: "3"),
    RadioGroup(index: 2, text: "2"),
    RadioGroup(index: 1, text: "1"),
  ];

  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    if (count == 0) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        DukunganSosial.getDukunganSosial(storage.getItem("id")).then((value) {
          result = value;
          if (result.success == 'ya') {
            setState(() {
              if (result.kekerasan == "ya") {
                _rgProgramming0 = 1;
              } else {
                _rgProgramming0 = 0;
              }
              _rgProgramming1 = int.parse(result.k1);
              _rgProgramming2 = int.parse(result.k2);
              _rgProgramming3 = int.parse(result.k3);
              _rgProgramming4 = int.parse(result.k4);
              _rgProgramming5 = int.parse(result.k5);
              _rgProgramming6 = int.parse(result.k6);
            });
          }
        });
        count++;
      });
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Dukungan Sosial"),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: ListView(
              children: <Widget>[
                Text("-> Pernahkah mengalami kekerasan dalam rumah tangga ?",
                    textAlign: TextAlign.left,
                    style: blackTextFont.copyWith(fontSize: 20)),
                _buildRadioButton(0),
                SizedBox(
                  height: 20.0,
                ),
                Text("-> Kuesioner dukungan sosial",
                    textAlign: TextAlign.left,
                    style: blackTextFont.copyWith(fontSize: 20)),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "1. Saya memiliki teman yang sangat mendukung saya",
                  textAlign: TextAlign.left,
                ),
                _buildRadioButton(1),
                SizedBox(
                  height: 20.0,
                ),
                Text("2. Keluarga sata selalu ada untuk saya"),
                _buildRadioButton(2),
                SizedBox(
                  height: 20.0,
                ),
                Text("3. Suami saya sangat membantu"),
                _buildRadioButton(3),
                SizedBox(
                  height: 20.0,
                ),
                Text("4. Saya memiliki konflik dengan suami"),
                _buildRadioButton(4),
                SizedBox(
                  height: 20.0,
                ),
                Text("5. Saya merasa dikendalikan oleh suami"),
                _buildRadioButton(5),
                SizedBox(
                  height: 20.0,
                ),
                Text("6. Saya merasa dicintai oleh suami"),
                _buildRadioButton(6),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "Simpan",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        status = true;

                        if (_rgProgramming0 == -1) {
                          status = false;
                          nomor = "0";
                        }
                        if (_rgProgramming1 == -1) {
                          status = false;
                          nomor = "1";
                        }
                        if (_rgProgramming2 == -1) {
                          status = false;
                          nomor = "2";
                        }
                        if (_rgProgramming3 == -1) {
                          status = false;
                          nomor = "3";
                        }
                        if (_rgProgramming4 == -1) {
                          status = false;
                          nomor = "4";
                        }
                        if (_rgProgramming5 == -1) {
                          status = false;
                          nomor = "5";
                        }
                        if (_rgProgramming6 == -1) {
                          status = false;
                          nomor = "6";
                        }

                        if (status == false) {
                          if (nomor == "0") {
                            Flushbar(
                              duration: Duration(seconds: 4),
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: mainColor,
                              message:
                                  "Jawab Soal Paling Atas Lebih dahulu !!!",
                            )..show(context);
                          } else {
                            Flushbar(
                              duration: Duration(seconds: 4),
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: mainColor,
                              message: "Jawab Soal Nomor " +
                                  nomor +
                                  " Lebih dahulu !!!",
                            )..show(context);
                          }
                        } else {
                          DukunganSosial.apiSubmitDukunganSosial(
                                  storage.getItem("id"),
                                  _rgProgramming0.toString(),
                                  _rgProgramming1.toString(),
                                  _rgProgramming2.toString(),
                                  _rgProgramming3.toString(),
                                  _rgProgramming4.toString(),
                                  _rgProgramming5.toString(),
                                  _rgProgramming6.toString())
                              .then((value) => {
                                    if (value.success == "ya")
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: greenColor,
                                          message: "Simpan Data Berhasil",
                                        )..show(context)
                                      }
                                    else
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: mainColor,
                                          message:
                                              "Simpan Data Gagal, Coba Lagi !",
                                        )..show(context)
                                      }
                                  });
                        }
                      }),
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: RaisedButton(
                      child: Text(
                        "Kembali",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: colorPurple,
                      onPressed: () {
                        storage.setItem("page", 0);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage();
                        }));
                      }),
                )
              ],
            ),
          ),
        ));
  }

  // widget radio
  Widget _buildRadioButton(parameter) {
    if (parameter == 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _kekerasan
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming0,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming0 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 1) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming1,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming1 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 2) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming2,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming2 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 3) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming3,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming3 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 4) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming4,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming4 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 5) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming5,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming5 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 6) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming6,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming6 = value;
                    });
                  },
                ))
            .toList(),
      );
    }
  }
}
