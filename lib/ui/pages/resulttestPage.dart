part of 'pages.dart';

class ResulttestPage extends StatefulWidget {
  @override
  _ResulttestPageState createState() => _ResulttestPageState();
}

class _ResulttestPageState extends State<ResulttestPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  @override
  Widget build(BuildContext context) {
    final LocalStorage storage = new LocalStorage('carebumil');
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              title: Text("TES EPDS SUKSES"),
              centerTitle: true,
              backgroundColor: mainColor,
            ),
            body: Container(
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 150,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/logo.png"))),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 70, bottom: 16),
                        child: Text(
                          "Terimakasih Ibu " + storage.getItem("nama"),
                          style: blackTextFont.copyWith(fontSize: 20),
                        ),
                      ),
                      Text(
                        "Telah Menyelesaikan Test Hari ini, datang lagi di Hari yang lain atau cek hasil test pada menu Hasil Test",
                        style: greyTextFont.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        width: 350,
                        height: 46,
                        margin: EdgeInsets.only(top: 150),
                        child: RaisedButton(
                            child: Text(
                              "Pergi Lihat Hasil Test",
                              style: whiteTextFont.copyWith(fontSize: 16),
                            ),
                            color: mainColor,
                            onPressed: () {
                              storage.setItem("page", 2);
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return HomePage();
                              }));
                            }),
                      ),
                      Container(
                        width: 350,
                        height: 46,
                        margin: EdgeInsets.only(top: 10),
                        child: RaisedButton(
                            child: Text(
                              "Kembali",
                              style: whiteTextFont.copyWith(fontSize: 16),
                            ),
                            color: colorPurple,
                            onPressed: () {
                              storage.setItem("page", 1);
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return HomePage();
                              }));
                            }),
                      )
                    ]))));
  }
}
