part of 'pages.dart';

class RiwayatPage extends StatefulWidget {
  @override
  _RiwayatPageState createState() => _RiwayatPageState();
}

class _RiwayatPageState extends State<RiwayatPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  Map<DateTime, double> line = {};
  LineChart chart;
  String status = null;
  int count = 0;

  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    int i = 1;
    Test.getResultTest(storage.getItem('id')).then((value) {
      print(value.success);
      if (value.success == 'ya') {
        status = "sukses";
      }
      value.data.forEach((key) {
        print(
            i.toString() + " = " + key["tanggal"] + ", data = " + key["score"]);
        line[DateTime.parse(key["tanggal"])] = double.parse(key["score"]);
        i++;
      });
    });

    if (count == 0) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          chart = AreaLineChart.fromDateTimeMaps(
              [line], [Colors.red.shade900], ['Skor'],
              gradients: [Pair(Colors.yellow.shade300, Colors.red.shade700)]);
        });
        count++;
      });
    }

    if (status == null) {
      return Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 20),
          alignment: Alignment.topCenter,
          child: Text(
            "Riwayat Test",
            style: blackTextFont.copyWith(fontSize: 20),
          ),
        ),
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage("assets/nodata.png"))),
        )
      ]);
    } else {
      return Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 20),
          alignment: Alignment.topCenter,
          child: Text(
            "Riwayat Test",
            style: blackTextFont.copyWith(fontSize: 20),
          ),
        ),
        Container(
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(padding: const EdgeInsets.all(30)),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AnimatedLineChart(
                    chart,
                    key: UniqueKey(),
                  ), //Unique key to force animations
                )),
                SizedBox(width: 200, height: 50, child: Text('')),
              ]),
        ),
      ]);
    }
  }
}
