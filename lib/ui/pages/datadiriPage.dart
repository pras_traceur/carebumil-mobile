part of 'pages.dart';

class DatadiriPage extends StatefulWidget {
  @override
  _DatadiriPageState createState() => _DatadiriPageState();
}

class _DatadiriPageState extends State<DatadiriPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  bool status = true;
  String nomor = "";
  String tanggal;
  String umur = "-";
  DataDiri result;
  int count = 0;
  DateTime _dateTime;
  DateFormat formatter = DateFormat('yyyy-MM-dd');
  String dropdownValue2 = 'SD';
  String dropdownValue3 = 'Bekerja';
  String dropdownValue4 = 'Menikah';
  String dropdownValue5 = 'Keluarga inti (suami,istri, anak)';
  String dropdownValue6 = 'Cukup';
  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    if (count == 0) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        DataDiri.getDataDiri(storage.getItem("id")).then((value) {
          result = value;
          if (result.success == 'ya') {
            setState(() {
              umur = result.umur;
              tanggal = result.tanggallahir;
              if (result.pendidikan == "0") {
                dropdownValue2 = 'SD';
              } else if (result.pendidikan == "1") {
                dropdownValue2 = 'SMP';
                print(result.pendidikan);
              } else if (result.pendidikan == "2") {
                dropdownValue2 = 'SMA';
              } else if (result.pendidikan == "3") {
                dropdownValue2 = 'S1';
              } else if (result.pendidikan == "4") {
                dropdownValue2 = 'S2';
              }

              if (result.pekerjaan == "0") {
                dropdownValue3 = 'Bekerja';
              } else if (result.pekerjaan == "1") {
                dropdownValue3 = 'Tidak Bekerja';
              }

              if (result.pernikahan == "0") {
                dropdownValue4 = 'Menikah';
              } else if (result.pernikahan == "1") {
                dropdownValue4 = 'Tidak Menikah';
              }

              if (result.tipekeluarga == "0") {
                dropdownValue5 = 'Keluarga inti (suami,istri, anak)';
              } else if (result.tipekeluarga == "1") {
                dropdownValue5 = 'Keluarga inti + keluarga tambahan';
              }

              if (result.penghasilan == "0") {
                dropdownValue6 = 'Cukup';
              } else if (result.penghasilan == "1") {
                dropdownValue6 = 'Tidak Cukup';
              }
            });
          }
        });

        count++;
      });
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Data Diri"),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: ListView(
              children: <Widget>[
                Text(tanggal == null
                    ? "1. Tanggal Lahir : Belum Memilih Hari"
                    : "1. Tanggal Lahir : " + tanggal),
                Text("Umur Anda : " + umur),
                RaisedButton(
                    child: Text(
                      "Ubah Tanggal Lahir",
                      style: whiteTextFont.copyWith(fontSize: 16),
                    ),
                    color: colorPurple,
                    onPressed: () {
                      showDatePicker(
                              context: context,
                              initialDate: _dateTime == null
                                  ? DateTime.now()
                                  : _dateTime,
                              firstDate: DateTime(1980),
                              lastDate: DateTime(2021))
                          .then((date) {
                        setState(() {
                          _dateTime = date;
                          tanggal = formatter.format(date);
                        });
                      });
                    }),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "2. Pendidikan terakhir",
                  textAlign: TextAlign.left,
                ),
                _buildDropdown(2, dropdownValue2),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "3. Pekerjaan",
                  textAlign: TextAlign.left,
                ),
                _buildDropdown(3, dropdownValue3),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "4. Status pernikahan",
                  textAlign: TextAlign.left,
                ),
                _buildDropdown(4, dropdownValue4),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "5. Tipe Keluarga",
                  textAlign: TextAlign.left,
                ),
                _buildDropdown(5, dropdownValue5),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "6. Apakah penghasilan keluarga cukup untuk memenuhi kebutuhan ?",
                  textAlign: TextAlign.left,
                ),
                _buildDropdown(6, dropdownValue6),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "Simpan",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        status = true;

                        if (tanggal == null) {
                          status = false;
                          nomor = "1";
                        }

                        if (status == false) {
                          Flushbar(
                            duration: Duration(seconds: 4),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: mainColor,
                            message: "Jawab Soal Nomor " +
                                nomor +
                                " Lebih dahulu !!!",
                          )..show(context);
                        } else {
                          DataDiri.apiSubmitDataDiri(
                                  storage.getItem("id"),
                                  tanggal,
                                  dropdownValue2,
                                  dropdownValue3,
                                  dropdownValue4,
                                  dropdownValue5,
                                  dropdownValue6)
                              .then((value) => {
                                    if (value.success == "ya")
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: greenColor,
                                          message: "Simpan Data Berhasil",
                                        )..show(context)
                                      }
                                    else
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: mainColor,
                                          message:
                                              "Simpan Data Gagal, Coba Lagi !",
                                        )..show(context)
                                      }
                                  });
                        }
                      }),
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: RaisedButton(
                      child: Text(
                        "Kembali",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: colorPurple,
                      onPressed: () {
                        storage.setItem("page", 0);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage();
                        }));
                      }),
                )
              ],
            ),
          ),
        ));
  }

  Widget _buildDropdown(angka, parameter) {
    if (angka == 2) {
      return DropdownButton<String>(
        value: parameter,
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue2 = newValue;
          });
        },
        items: <String>['SD', 'SMP', 'SMA', 'S1', 'S2']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      );
    } else if (angka == 3) {
      return DropdownButton<String>(
        value: parameter,
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue3 = newValue;
          });
        },
        items: <String>['Bekerja', 'Tidak Bekerja']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      );
    } else if (angka == 4) {
      return DropdownButton<String>(
        value: parameter,
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue4 = newValue;
          });
        },
        items: <String>['Menikah', 'Tidak Menikah']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      );
    } else if (angka == 5) {
      return DropdownButton<String>(
        value: parameter,
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue5 = newValue;
          });
        },
        items: <String>[
          'Keluarga inti (suami,istri, anak)',
          'Keluarga inti + keluarga tambahan'
        ].map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      );
    } else if (angka == 6) {
      return DropdownButton<String>(
        value: parameter,
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue6 = newValue;
          });
        },
        items: <String>['Cukup', 'Tidak Cukup']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      );
    }
  }
}
