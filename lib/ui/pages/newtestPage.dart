part of 'pages.dart';

class NewtestPage extends StatefulWidget {
  @override
  _NewtestPageState createState() => _NewtestPageState();
}

class _NewtestPageState extends State<NewtestPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  int _rgProgramming1 = -1;
  int _rgProgramming2 = -1;
  int _rgProgramming3 = -1;
  int _rgProgramming4 = -1;
  int _rgProgramming5 = -1;
  int _rgProgramming6 = -1;
  int _rgProgramming7 = -1;
  int _rgProgramming8 = -1;
  int _rgProgramming9 = -1;
  int _rgProgramming10 = -1;

  bool status = true;
  String nomor = "";
  String _selectedValue;

  List<RadioGroup> _programmingList = null;

  final List<RadioGroup> _programmingList1 = [
    RadioGroup(index: 0, text: "Sebanyak yang saya bisa"),
    RadioGroup(index: 1, text: "Tidak terlalu banyak"),
    RadioGroup(index: 2, text: "Tidak banyak"),
    RadioGroup(index: 3, text: "Tidak sama sekali"),
  ];

  final List<RadioGroup> _programmingList2 = [
    RadioGroup(index: 0, text: "Sebanyak sebelumya"),
    RadioGroup(
        index: 1, text: "Agak sedikit kurang dibandingkan dengan sebelumnya"),
    RadioGroup(index: 2, text: "Kurang dibandingkan dengan sebelumnya"),
    RadioGroup(index: 3, text: "Tidak pernah sama sekali"),
  ];

  final List<RadioGroup> _programmingList3 = [
    RadioGroup(index: 3, text: "Ya, setiap saat"),
    RadioGroup(index: 2, text: "Ya, kadang-kadang"),
    RadioGroup(index: 1, text: "Tidak terlalu sering"),
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
  ];

  final List<RadioGroup> _programmingList4 = [
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
    RadioGroup(index: 1, text: "Jarang-jarang"),
    RadioGroup(index: 2, text: "Ya, kadang-kadang"),
    RadioGroup(index: 3, text: "Ya, sering sekali"),
  ];

  final List<RadioGroup> _programmingList5 = [
    RadioGroup(index: 3, text: "Ya, cukup sering"),
    RadioGroup(index: 2, text: "Ya, kadang-kadang"),
    RadioGroup(index: 1, text: "Tidak terlalu sering"),
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
  ];

  final List<RadioGroup> _programmingList6 = [
    RadioGroup(
        index: 3, text: "Ya, hampir setiap saat saya tidak mampu menanganinya"),
    RadioGroup(
        index: 2,
        text: "Ya, kadang-kadang saya tidak mampu menangani seperti biasanya"),
    RadioGroup(
        index: 1, text: "Tidak terlalu, sebagian besar berhasil saya tangani"),
    RadioGroup(
        index: 0,
        text:
            "Tidak pernah, saya mampu mengerjakan segala sesuatu dengan baik"),
  ];

  final List<RadioGroup> _programmingList7 = [
    RadioGroup(index: 3, text: "Ya, setiap saat"),
    RadioGroup(index: 2, text: "Ya, kadang-kadang"),
    RadioGroup(index: 1, text: "Tidak terlalu sering"),
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
  ];

  final List<RadioGroup> _programmingList8 = [
    RadioGroup(index: 3, text: "Ya, setiap saat"),
    RadioGroup(index: 2, text: "Ya, cukup sering"),
    RadioGroup(index: 1, text: "Tidak terlalu sering"),
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
  ];

  final List<RadioGroup> _programmingList9 = [
    RadioGroup(index: 3, text: "Ya, setiap saat"),
    RadioGroup(index: 2, text: "Ya, cukup sering"),
    RadioGroup(index: 1, text: "Disaat tertentu saja"),
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
  ];

  final List<RadioGroup> _programmingList10 = [
    RadioGroup(index: 3, text: "Ya, cukup sering"),
    RadioGroup(index: 2, text: "Kadang-kadang"),
    RadioGroup(index: 1, text: "Jarang sekali"),
    RadioGroup(index: 0, text: "Tidak pernah sama sekali"),
  ];

  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("TES EPDS"),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: ListView(
              children: <Widget>[
                Text(
                  "1. Dalam 7 hari terakhir, Saya mampu tertawa dan merasakan hal-hal yang menyenangkan",
                  textAlign: TextAlign.left,
                ),
                _buildRadioButton(1),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "2. Dalam 7 hari terakhir, Saya melihat segala sesuatunya kedepan sangat menyenangkan"),
                _buildRadioButton(2),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "3. Dalam 7 hari terakhir, Saya menyalahkan diri saya sendiri saat sesuatu terjadi tidak sebagaimana mestinya"),
                _buildRadioButton(3),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "4. Dalam 7 hari terakhir, Saya merasa cemas atau merasa kuatir tanpa alasan yang jelas"),
                _buildRadioButton(4),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "5. Dalam 7 hari terakhir, Saya merasa takut atau panik tanpa alasan yang jelas"),
                _buildRadioButton(5),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "6. Dalam 7 hari terakhir, Segala sesuatunya terasa sulit untuk dikerjakan"),
                _buildRadioButton(6),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "7. Dalam 7 hari terakhir, Saya merasa tidak bahagia sehingga mengalami kesulitan untuk tidur"),
                _buildRadioButton(7),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "8. Dalam 7 hari terakhir, Saya merasa sedih dan merasa diri saya menyedihkan"),
                _buildRadioButton(8),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "9. Dalam 7 hari terakhir, Saya merasa tidak bahagia sehingga menyebabkan saya menangis"),
                _buildRadioButton(9),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                    "10. Dalam 7 hari terakhir, Muncul pikiran untuk menyakiti diri saya sendiri"),
                _buildRadioButton(10),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "Proses",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        status = true;
                        if (_rgProgramming1 == -1) {
                          status = false;
                          nomor = "1";
                        }
                        if (_rgProgramming2 == -1) {
                          status = false;
                          nomor = "2";
                        }
                        if (_rgProgramming3 == -1) {
                          status = false;
                          nomor = "3";
                        }
                        if (_rgProgramming4 == -1) {
                          status = false;
                          nomor = "4";
                        }
                        if (_rgProgramming5 == -1) {
                          status = false;
                          nomor = "5";
                        }
                        if (_rgProgramming6 == -1) {
                          status = false;
                          nomor = "6";
                        }
                        if (_rgProgramming7 == -1) {
                          status = false;
                          nomor = "7";
                        }
                        if (_rgProgramming8 == -1) {
                          status = false;
                          nomor = "8";
                        }
                        if (_rgProgramming9 == -1) {
                          status = false;
                          nomor = "9";
                        }
                        if (_rgProgramming10 == -1) {
                          status = false;
                          nomor = "10";
                        }

                        if (status == false) {
                          Flushbar(
                            duration: Duration(seconds: 4),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: mainColor,
                            message: "Jawab Soal Nomor " +
                                nomor +
                                " Lebih dahulu !!!",
                          )..show(context);
                        } else {
                          Test.apiSubmitTest(
                                  storage.getItem("id"),
                                  _rgProgramming1.toString(),
                                  _rgProgramming2.toString(),
                                  _rgProgramming3.toString(),
                                  _rgProgramming4.toString(),
                                  _rgProgramming5.toString(),
                                  _rgProgramming6.toString(),
                                  _rgProgramming7.toString(),
                                  _rgProgramming8.toString(),
                                  _rgProgramming9.toString(),
                                  _rgProgramming10.toString())
                              .then((value) => {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return ResulttestPage();
                                    }))
                                  });
                        }
                      }),
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: RaisedButton(
                      child: Text(
                        "Kembali",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: colorPurple,
                      onPressed: () {
                        storage.setItem("page", 0);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage();
                        }));
                      }),
                )
              ],
            ),
          ),
        ));
  }

  // widget radio
  Widget _buildRadioButton(parameter) {
    if (parameter == 1) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList1
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming1,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming1 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 2) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList2
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming2,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming2 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 3) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList3
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming3,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming3 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 4) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList4
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming4,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming4 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 5) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList5
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming5,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming5 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 6) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList6
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming6,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming6 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 7) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList7
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming7,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming7 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 8) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList8
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming8,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming8 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 9) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList9
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming9,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming9 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 10) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _programmingList10
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming10,
                  controlAffinity: ListTileControlAffinity.trailing,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming10 = value;
                    });
                  },
                ))
            .toList(),
      );
    }
  }
}

class RadioGroup {
  final int index;
  final String text;
  RadioGroup({this.index, this.text});
}
