part of 'pages.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  Pengguna result = null;
  String success = null;
  TextEditingController namaController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController rekamMedikController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isNamaValid = false;
  bool isEmailValid = false;
  bool isRekamMedikValid = false;
  bool isPasswordValid = false;
  bool isSignupValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0),
            child: AppBar(
              // Here we create one to set status bar color
              backgroundColor:
                  Colors.black, // Set any color of status bar you want;
            )),
        backgroundColor: Colors.white,
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: ListView(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 50, bottom: 10),
                      child: Text(
                        "Daftar Akun Baru",
                        style: blackTextFont.copyWith(fontSize: 20),
                      ),
                    ),
                    Text(
                      "(Sistem Deteksi Depresi Antenatal)",
                      style: greyTextFont.copyWith(
                          fontSize: 16, fontWeight: FontWeight.w300),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    SizedBox(
                      height: 80,
                      child: Image.asset("assets/logo.png"),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    TextField(
                      onChanged: (text) {
                        setState(() {
                          isNamaValid = text.length >= 1;
                        });
                      },
                      controller: namaController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Nama Pengguna",
                          hintText: "Nama Pengguna"),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextField(
                      onChanged: (text) {
                        setState(() {
                          isEmailValid = EmailValidator.validate(text);
                        });
                      },
                      controller: emailController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Email Pengguna",
                          hintText: "Email Pengguna"),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextField(
                      onChanged: (text) {
                        setState(() {
                          isRekamMedikValid = text.length >= 1;
                        });
                      },
                      controller: rekamMedikController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Nomor Rekam Medik",
                          hintText: "Nomor Rekam Medik"),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextField(
                      onChanged: (text) {
                        setState(() {
                          isPasswordValid = text.length >= 6;
                        });
                      },
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Password",
                          hintText: "Password"),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      width: 350,
                      height: 46,
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: RaisedButton(
                        child: Text(
                          "Daftar",
                          style: whiteTextFont.copyWith(fontSize: 16),
                        ),
                        color: mainColor,
                        onPressed: isNamaValid &&
                                isEmailValid &&
                                isRekamMedikValid &&
                                isPasswordValid
                            ? () async {
                                await Pengguna.signUpApi(
                                        namaController.text,
                                        emailController.text,
                                        rekamMedikController.text,
                                        passwordController.text)
                                    .then((value) {
                                  result = value;
                                  if (result.success == 'ya') {
                                    Flushbar(
                                      duration: Duration(seconds: 4),
                                      flushbarPosition: FlushbarPosition.TOP,
                                      backgroundColor: greenColor,
                                      message:
                                          "Pendaftaran Berhasil, Silahkan Login menggunakan Emal & Password Anda...",
                                    )..show(context);
                                  } else {
                                    Flushbar(
                                      duration: Duration(seconds: 4),
                                      flushbarPosition: FlushbarPosition.TOP,
                                      backgroundColor: mainColor,
                                      message: "Daftar Gagal !",
                                    )..show(context);
                                  }
                                });
                              }
                            : null,
                      ),
                    ),
                    Container(
                      width: 350,
                      height: 46,
                      margin: EdgeInsets.only(top: 0, bottom: 0),
                      child: RaisedButton(
                          child: Text(
                            "Sudah Punya Akun",
                            style: whiteTextFont.copyWith(fontSize: 16),
                          ),
                          color: colorPurple,
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return LoginPage();
                            }));
                          }),
                    )
                  ],
                ),
              ],
            )));
  }
}
