part of 'pages.dart';

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  Test result;
  bool status = false;
  @override
  Widget build(BuildContext context) {
    Test.getLastTest(storage.getItem('id')).then((value) {
      result = value;
      if (result.success == 'ya') {
        final DateTime now = DateTime.now();
        final DateFormat formatter = DateFormat('yyyy-MM-dd');
        final String formatted = formatter.format(now);
        if (result.tanggal == formatted) {
          status = true;
        }
      }
    });
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    return Stack(children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 20),
        alignment: Alignment.topCenter,
        child: Text(
          "Test Deteksi Depresi",
          style: blackTextFont.copyWith(fontSize: 20),
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 70),
        height: 250,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("assets/test.png"))),
      ),
      Container(
        margin: EdgeInsets.only(top: 330),
        alignment: Alignment.topCenter,
        child: Text(
          "TES EPDS",
          style: blackTextFont.copyWith(fontSize: 30),
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 370),
        alignment: Alignment.topCenter,
        child: Text(
          "Test Edinburgh Postnatal Depression\nScale (EPDS) adalah tes deteksi depresi\npada ibu hamil dan nifas. Tes ini terdiri\ndari 10 pertanyaan yang harus ibu\njawab dalam satu waktu sesuai dengan\napa yang ibu rasakan.",
          style: greyTextFont.copyWith(fontSize: 13),
          textAlign: TextAlign.center,
        ),
      ),
      Center(
        child: Container(
          width: 250,
          height: 46,
          margin: EdgeInsets.only(top: 400, bottom: 10),
          child: RaisedButton(
              child: Text(
                "Mulai Test",
                style: whiteTextFont.copyWith(fontSize: 16),
                textAlign: TextAlign.center,
              ),
              color: mainColor,
              onPressed: () {
                if (status == false) {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return NewtestPage();
                  }));
                } else {
                  Flushbar(
                    duration: Duration(seconds: 4),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: mainColor,
                    message:
                        "Anda Telah Melakukan Test Hari ini datang lagi di Hari yang lain",
                  )..show(context);
                }
              }),
        ),
      ),
    ]);
  }
}
