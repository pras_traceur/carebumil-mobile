part of 'pages.dart';

class RiwayatobstetriPage extends StatefulWidget {
  @override
  _RiwayatobstetriPageState createState() => _RiwayatobstetriPageState();
}

class _RiwayatobstetriPageState extends State<RiwayatobstetriPage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  TextEditingController jikaController = TextEditingController();
  int _rgProgramming1 = 0;
  int _rgProgramming2 = 0;
  int _rgProgramming3 = 0;
  int _rgProgramming4 = -1;
  int _rgProgramming5 = 0;

  bool status = true;
  String nomor = "";
  String coment = "";
  String _selectedValue;
  RiwayatObstetri result;
  int count = 0;

  bool isJika = false;

  final List<RadioGroup> _obstetri = [
    RadioGroup(index: 1, text: "Ya"),
    RadioGroup(index: 0, text: "Tidak"),
  ];

  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    if (count == 0) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        RiwayatObstetri.getRiwayatObstetri(storage.getItem("id")).then((value) {
          result = value;
          if (result.success == 'ya') {
            setState(() {
              if (result.k1 == "ya") {
                _rgProgramming1 = 1;
              } else {
                _rgProgramming1 = 0;
              }
              if (result.k2 == "ya") {
                _rgProgramming2 = 1;
              } else {
                _rgProgramming2 = 0;
              }
              if (result.k3 == "ya") {
                _rgProgramming3 = 1;
              } else {
                _rgProgramming3 = 0;
              }
              if (result.k4 == "ya") {
                _rgProgramming4 = 1;
              } else {
                _rgProgramming4 = 0;
              }
              if (result.k5 == "ya") {
                _rgProgramming5 = 1;
              } else {
                _rgProgramming5 = 0;
              }
              jikaController.text = result.jikaya;
            });
          }
        });
        count++;
      });
    }
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Riwayat Obstetri"),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: ListView(
              children: <Widget>[
                _alertIcon(_rgProgramming1),
                Text(
                  "1. Pernah depresi sebelumnya selama hamil/setelah persalinan ?",
                  textAlign: TextAlign.left,
                ),
                _buildRadioButton(1),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming2),
                Text("2. Pernah keguguran ?"),
                _buildRadioButton(2),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming3),
                Text("3. Pernah bersalin caesar/dengan tindakan ?"),
                _buildRadioButton(3),
                SizedBox(
                  height: 20.0,
                ),
                Text("4. Jika ya apakah direncanakan ?"),
                _buildRadioButton(4),
                SizedBox(
                  height: 20.0,
                ),
                _alertIcon(_rgProgramming5),
                Text(
                    "5. Pernah mengalami komplikasi/masalah kehamilan/persalinan/nifas terdahulu ?"),
                _buildRadioButton(5),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "6. Jika Ya Sebutkan:",
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  onChanged: (text) {
                    setState(() {
                      isJika = text.length >= 1;
                    });
                  },
                  controller: jikaController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: "Wajib diisi...",
                      hintText: "Isi Keluhan/Masalah"),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: RaisedButton(
                      child: Text(
                        "Simpan",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: mainColor,
                      onPressed: () {
                        status = true;

                        if (_rgProgramming1 == -1) {
                          status = false;
                          nomor = "1";
                        }
                        if (_rgProgramming2 == -1) {
                          status = false;
                          nomor = "2";
                        }
                        if (_rgProgramming3 == -1) {
                          status = false;
                          nomor = "3";
                        }
                        if (_rgProgramming4 == -1) {
                          status = false;
                          nomor = "4";
                        }
                        if (_rgProgramming5 == -1) {
                          status = false;
                          nomor = "5";
                        }

                        if (jikaController.text.length < 1) {
                          status = false;
                          nomor = "6";
                        }

                        if (status == false) {
                          Flushbar(
                            duration: Duration(seconds: 4),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: mainColor,
                            message: "Jawab Soal Nomor " +
                                nomor +
                                " Lebih dahulu !!!",
                          )..show(context);
                        } else {
                          RiwayatObstetri.apiSubmitRiwayatObstetri(
                                  storage.getItem("id"),
                                  _rgProgramming1.toString(),
                                  _rgProgramming2.toString(),
                                  _rgProgramming3.toString(),
                                  _rgProgramming4.toString(),
                                  _rgProgramming5.toString(),
                                  jikaController.text)
                              .then((value) => {
                                    if (value.success == "ya")
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: greenColor,
                                          message: "Simpan Data Berhasil",
                                        )..show(context)
                                      }
                                    else
                                      {
                                        Flushbar(
                                          duration: Duration(seconds: 4),
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          backgroundColor: mainColor,
                                          message:
                                              "Simpan Data Gagal, Coba Lagi !",
                                        )..show(context)
                                      }
                                  });
                        }
                      }),
                ),
                Container(
                  width: 350,
                  height: 46,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: RaisedButton(
                      child: Text(
                        "Kembali",
                        style: whiteTextFont.copyWith(fontSize: 16),
                      ),
                      color: colorPurple,
                      onPressed: () {
                        storage.setItem("page", 0);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage();
                        }));
                      }),
                )
              ],
            ),
          ),
        ));
  }

  Widget _alertIcon(parameter) {
    if (parameter == 1) {
      return Icon(
        MdiIcons.alert,
        color: Colors.red,
      );
    } else {
      return Container();
    }
  }

  // widget radio
  Widget _buildRadioButton(parameter) {
    if (parameter == 1) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _obstetri
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming1,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming1 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 2) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _obstetri
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming2,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming2 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 3) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _obstetri
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming3,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming3 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 4) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _obstetri
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming4,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming4 = value;
                    });
                  },
                ))
            .toList(),
      );
    } else if (parameter == 5) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _obstetri
            .map((programming) => RadioListTile(
                  title: Text(programming.text),
                  value: programming.index,
                  groupValue: _rgProgramming5,
                  dense: true,
                  onChanged: (value) {
                    setState(() {
                      _rgProgramming5 = value;
                    });
                  },
                ))
            .toList(),
      );
    }
  }
}
