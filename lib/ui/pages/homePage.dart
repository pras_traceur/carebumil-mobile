part of 'pages.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final LocalStorage storage = new LocalStorage('carebumil');
  int bottomNav;
  Pengguna result = null;
  String nama = null;
  PageController pageController;
  @override
  void initState() {
    super.initState();
    if (storage.getItem("page") != null) {
      bottomNav = int.parse(storage.getItem("page").toString());
    } else {
      bottomNav = 0;
    }
    pageController = PageController(initialPage: bottomNav);
  }

  void changePage(param) {
    bottomNav = param;
    pageController.jumpToPage(param);
    print(bottomNav);
  }

  @override
  Widget build(BuildContext context) {
    if (storage.getItem('login') == '0') {
      return LoginPage();
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title:
                Image.asset('assets/header.png', fit: BoxFit.cover, height: 30),
            centerTitle: true,
            backgroundColor: mainColor,
          ),
          body: Stack(
            children: <Widget>[
              Container(
                color: Color(0xFFF7F7F7),
              ),
              PageView(
                controller: pageController,
                onPageChanged: (index) {
                  setState(() {
                    bottomNav = index;
                  });
                },
                children: <Widget>[
                  TentangPage(),
                  TestPage(),
                  HasiltestPage(),
                  RiwayatPage(),
                  AkunPage()
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 70,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Color(0xFFE4E4E4)),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: BottomNavigationBar(
                      elevation: 0,
                      backgroundColor: Colors.transparent,
                      selectedItemColor: mainColor,
                      unselectedItemColor: colorGrey,
                      currentIndex: bottomNav,
                      type: BottomNavigationBarType.fixed,
                      onTap: (index) {
                        setState(() {
                          bottomNav = index;
                          pageController.jumpToPage(index);
                        });
                      },
                      items: [
                        BottomNavigationBarItem(
                            title: Text(
                              "Tentangku",
                              style: GoogleFonts.raleway(
                                  fontSize: 13, fontWeight: FontWeight.w600),
                            ),
                            icon: Container(
                              margin: EdgeInsets.only(bottom: 6),
                              height: 20,
                              child: Icon(
                                MdiIcons.humanPregnant,
                                color: (bottomNav == 0) ? mainColor : colorGrey,
                              ),
                            )),
                        BottomNavigationBarItem(
                            title: Text(
                              "Test",
                              style: GoogleFonts.raleway(
                                  fontSize: 13, fontWeight: FontWeight.w600),
                            ),
                            icon: Container(
                              margin: EdgeInsets.only(bottom: 6),
                              height: 20,
                              child: Icon(
                                MdiIcons.testTube,
                                color: (bottomNav == 1) ? mainColor : colorGrey,
                              ),
                            )),
                        BottomNavigationBarItem(
                            title: Text(
                              "Hasil Tes",
                              style: GoogleFonts.raleway(
                                  fontSize: 13, fontWeight: FontWeight.w600),
                            ),
                            icon: Container(
                              margin: EdgeInsets.only(bottom: 6),
                              height: 20,
                              child: Icon(
                                MdiIcons.chartLine,
                                color: (bottomNav == 2) ? mainColor : colorGrey,
                              ),
                            )),
                        BottomNavigationBarItem(
                            title: Text(
                              "Riwayat",
                              style: GoogleFonts.raleway(
                                  fontSize: 13, fontWeight: FontWeight.w600),
                            ),
                            icon: Container(
                              margin: EdgeInsets.only(bottom: 6),
                              height: 20,
                              child: Icon(
                                MdiIcons.history,
                                color: (bottomNav == 3) ? mainColor : colorGrey,
                              ),
                            )),
                        BottomNavigationBarItem(
                            title: Text(
                              "Akun",
                              style: GoogleFonts.raleway(
                                  fontSize: 13, fontWeight: FontWeight.w600),
                            ),
                            icon: Container(
                              margin: EdgeInsets.only(bottom: 6),
                              height: 20,
                              child: Icon(
                                MdiIcons.faceProfileWoman,
                                color: (bottomNav == 4) ? mainColor : colorGrey,
                              ),
                            )),
                      ]),
                ),
              )
            ],
          )),
    );
  }
}
