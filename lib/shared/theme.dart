part of 'shared.dart';

const double defaultMargin = 24;

Color mainColor = Color(0xFFFF4C8F);
Color colorGrey = Color(0xFFADADAD);
Color colorPurple = Color(0xFF503E9D);
Color greenColor = Color(0xFF3E9D9D);

TextStyle mainTextFont = GoogleFonts.raleway()
    .copyWith(color: mainColor, fontWeight: FontWeight.w500);
TextStyle blackTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.black, fontWeight: FontWeight.w500);
TextStyle whiteTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w500);
TextStyle purpleTextFont = GoogleFonts.raleway()
    .copyWith(color: colorPurple, fontWeight: FontWeight.w500);
TextStyle greyTextFont = GoogleFonts.raleway()
    .copyWith(color: colorGrey, fontWeight: FontWeight.w500);
TextStyle whiteNumberFont =
    GoogleFonts.openSans().copyWith(color: Colors.white);
TextStyle purpleNumberFont =
    GoogleFonts.openSans().copyWith(color: colorPurple);
