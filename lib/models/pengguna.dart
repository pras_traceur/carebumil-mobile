part of 'models.dart';

class Pengguna {
  final String idpengguna;
  final String idperanpengguna;
  final String namapengguna;
  final String username;
  final String password;
  final String foto;
  final String emailpengguna;
  final String tanggallahirpengguna;
  final String tempatlahirpengguna;
  final String alamatpengguna;
  final String teleponpengguna;
  final String jabatan;
  final String lihat;
  final String status;
  final String flag;
  final String dibuatoleh;
  final String tanggalbuat;
  final String tanggalubah;
  final String rekammedik;
  final String success;
  final String info;

  Pengguna({
    this.idpengguna,
    this.idperanpengguna,
    this.namapengguna,
    this.username,
    this.password,
    this.foto,
    this.emailpengguna,
    this.tanggallahirpengguna,
    this.tempatlahirpengguna,
    this.alamatpengguna,
    this.teleponpengguna,
    this.jabatan,
    this.lihat,
    this.status,
    this.flag,
    this.dibuatoleh,
    this.tanggalbuat,
    this.tanggalubah,
    this.rekammedik,
    this.success,
    this.info,
  });

  factory Pengguna.createPengguna(Map<String, dynamic> object) {
    return Pengguna(
      idpengguna: object['id_pengguna'],
      idperanpengguna: object['id_peran_pengguna'],
      namapengguna: object['nama_pengguna'],
      username: object['username'],
      password: object['password'],
      foto: object['foto'],
      emailpengguna: object['email_pengguna'],
      tanggallahirpengguna: object['tanggal_lahir_pengguna'],
      tempatlahirpengguna: object['tempat_lahir_pengguna'],
      alamatpengguna: object['alamat_pengguna'],
      teleponpengguna: object['telepon_pengguna'],
      jabatan: object['jabatan'],
      lihat: object['lihat'],
      status: object['status'],
      flag: object['flag'],
      dibuatoleh: object['dibuat_oleh'],
      tanggalbuat: object['tanggal_buat'],
      tanggalubah: object['tanggal_ubah'],
      rekammedik: object['rekam_medik'],
      success: object['success'],
      info: object['info'],
    );
  }

  static Future<Pengguna> loginApi(
      String emailpengguna, String password) async {
    String apiURL = "https://carebumil.com/api/loginApi";
    var apiResult = await http.post(apiURL,
        body: {"email_pengguna": emailpengguna, "password": password});
    var jsonObject = json.decode(apiResult.body);
    return Pengguna.createPengguna(jsonObject);
  }

  static Future<Pengguna> signUpApi(String namapengguna, String emailpengguna,
      String rekammedik, String password) async {
    String apiURL = "https://carebumil.com/api/signUpApi";
    var apiResult = await http.post(apiURL, body: {
      "email_pengguna": emailpengguna,
      "password": password,
      "nama_pengguna": namapengguna,
      "rekam_medik": rekammedik
    });
    var jsonObject = json.decode(apiResult.body);
    return Pengguna.createPengguna(jsonObject);
  }
}
