part of 'models.dart';

class Test {
  final String id;
  final String idpengguna;
  final String tanggal;
  final String score;
  final String flag;
  final String hasil;
  final String k1;
  final String k2;
  final String k3;
  final String k4;
  final String k5;
  final String k6;
  final String k7;
  final String k8;
  final String k9;
  final String k10;
  final String success;
  final String info;
  final List<dynamic> data;

  Test(
      {this.id,
      this.idpengguna,
      this.tanggal,
      this.score,
      this.flag,
      this.hasil,
      this.k1,
      this.k2,
      this.k3,
      this.k4,
      this.k5,
      this.k6,
      this.k7,
      this.k8,
      this.k9,
      this.k10,
      this.success,
      this.info,
      this.data});

  factory Test.createTest(Map<String, dynamic> object) {
    return Test(
      id: object['id'],
      idpengguna: object['id_pengguna'],
      tanggal: object['tanggal'],
      score: object['score'],
      flag: object['flag'],
      hasil: object['hasil'],
      k1: object['k1'],
      k2: object['k2'],
      k3: object['k3'],
      k4: object['k4'],
      k5: object['k5'],
      k6: object['k6'],
      k7: object['k7'],
      k8: object['k8'],
      k9: object['k9'],
      k10: object['k10'],
      success: object['success'],
      info: object['info'],
      data: object['data'],
    );
  }

  static Future<Test> getLastTest(String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getLastTest?id_pengguna=" + idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return Test.createTest(jsonObject);
  }

  static Future<Test> getResultTest(String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getResultTest?id_pengguna=" + idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return Test.createTest(jsonObject);
  }

  static Future<Test> apiSubmitTest(
      String idpengguna,
      String k1,
      String k2,
      String k3,
      String k4,
      String k5,
      String k6,
      String k7,
      String k8,
      String k9,
      String k10) async {
    String apiURL = "https://carebumil.com/api/apiSubmitTest";
    var apiResult = await http.post(apiURL, body: {
      "id_pengguna": idpengguna,
      "k1": k1,
      "k2": k2,
      "k3": k3,
      "k4": k4,
      "k5": k5,
      "k6": k6,
      "k7": k7,
      "k8": k8,
      "k9": k9,
      "k10": k10
    });
    var jsonObject = json.decode(apiResult.body);
    return Test.createTest(jsonObject);
  }
}
