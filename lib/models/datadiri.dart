part of 'models.dart';

class DataDiri {
  final String id;
  final String idpengguna;
  final String umur;
  final String tanggallahir;
  final String pendidikan;
  final String pekerjaan;
  final String pernikahan;
  final String tipekeluarga;
  final String penghasilan;
  final String success;
  final String info;

  DataDiri(
      {this.id,
      this.idpengguna,
      this.umur,
      this.tanggallahir,
      this.pendidikan,
      this.pekerjaan,
      this.pernikahan,
      this.tipekeluarga,
      this.penghasilan,
      this.success,
      this.info});

  factory DataDiri.createDataDiri(Map<String, dynamic> object) {
    return DataDiri(
        id: object['id'],
        idpengguna: object['id_pengguna'],
        umur: object['umur'],
        tanggallahir: object['tanggal_lahir'],
        pendidikan: object['pendidikan'],
        pekerjaan: object['pekerjaan'],
        pernikahan: object['pernikahan'],
        tipekeluarga: object['tipe_keluarga'],
        penghasilan: object['penghasilan'],
        success: object['success'],
        info: object['info']);
  }

  static Future<DataDiri> getDataDiri(String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getDataDiri?id_pengguna=" + idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return DataDiri.createDataDiri(jsonObject);
  }

  static Future<DataDiri> apiSubmitDataDiri(
      String idpengguna,
      String tanggallahir,
      String pendidikan,
      String pekerjaan,
      String pernikahan,
      String tipekeluarga,
      String penghasilan) async {
    String apiURL = "https://carebumil.com/api/apiSubmitDataDiri";
    var apiResult = await http.post(apiURL, body: {
      "id_pengguna": idpengguna,
      "tanggal_lahir": tanggallahir,
      "pendidikan": pendidikan,
      "pekerjaan": pekerjaan,
      "pernikahan": pernikahan,
      "tipe_keluarga": tipekeluarga,
      "penghasilan": penghasilan
    });
    var jsonObject = json.decode(apiResult.body);
    return DataDiri.createDataDiri(jsonObject);
  }
}
