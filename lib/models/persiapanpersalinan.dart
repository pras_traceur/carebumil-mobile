part of 'models.dart';

class PersiapanPersalinan {
  final String id;
  final String idpengguna;
  final String kelashamil;
  final String rencanapersalinan;
  final String di;
  final String keinginanbersalin;
  final String asuransi;
  final String persiapandana;
  final String success;
  final String info;

  PersiapanPersalinan(
      {this.id,
      this.idpengguna,
      this.kelashamil,
      this.rencanapersalinan,
      this.di,
      this.keinginanbersalin,
      this.asuransi,
      this.persiapandana,
      this.success,
      this.info});

  factory PersiapanPersalinan.createPersiapanPersalinan(
      Map<String, dynamic> object) {
    return PersiapanPersalinan(
        id: object['id'],
        idpengguna: object['id_pengguna'],
        kelashamil: object['kelas_hamil'],
        rencanapersalinan: object['rencana_persalinan'],
        di: object['di'],
        keinginanbersalin: object['keinginan_bersalin'],
        asuransi: object['asuransi'],
        persiapandana: object['persiapan_dana'],
        success: object['success'],
        info: object['info']);
  }

  static Future<PersiapanPersalinan> getPersiapanPersalinan(
      String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getPersiapanPersalinan?id_pengguna=" +
            idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return PersiapanPersalinan.createPersiapanPersalinan(jsonObject);
  }

  static Future<PersiapanPersalinan> apiSubmitPersiapanPersalinan(
      String idpengguna,
      String kelashamil,
      String rencanapersalinan,
      String di,
      String keinginanbersalin,
      String asuransi,
      String persiapandana) async {
    String apiURL = "https://carebumil.com/api/apiSubmitPersiapanPersalinan";
    var apiResult = await http.post(apiURL, body: {
      "id_pengguna": idpengguna,
      "kelas_hamil": kelashamil,
      "rencana_persalinan": rencanapersalinan,
      "di": di,
      "keinginan_bersalin": keinginanbersalin,
      "asuransi": asuransi,
      "persiapan_dana": persiapandana,
    });
    var jsonObject = json.decode(apiResult.body);
    return PersiapanPersalinan.createPersiapanPersalinan(jsonObject);
  }
}
