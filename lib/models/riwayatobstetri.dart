part of 'models.dart';

class RiwayatObstetri {
  final String id;
  final String idpengguna;
  final String jikaya;
  final String k1;
  final String k2;
  final String k3;
  final String k4;
  final String k5;
  final String success;
  final String info;

  RiwayatObstetri(
      {this.id,
      this.idpengguna,
      this.jikaya,
      this.k1,
      this.k2,
      this.k3,
      this.k4,
      this.k5,
      this.success,
      this.info});

  factory RiwayatObstetri.createRiwayatObstetri(Map<String, dynamic> object) {
    return RiwayatObstetri(
        id: object['id'],
        idpengguna: object['id_pengguna'],
        jikaya: object['jika_ya'],
        k1: object['k1'],
        k2: object['k2'],
        k3: object['k3'],
        k4: object['k4'],
        k5: object['k5'],
        success: object['success'],
        info: object['info']);
  }

  static Future<RiwayatObstetri> getRiwayatObstetri(String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getRiwayatObstetri?id_pengguna=" +
            idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return RiwayatObstetri.createRiwayatObstetri(jsonObject);
  }

  static Future<RiwayatObstetri> apiSubmitRiwayatObstetri(
      String idpengguna,
      String k1,
      String k2,
      String k3,
      String k4,
      String k5,
      String jikaya) async {
    String apiURL = "https://carebumil.com/api/apiSubmitRiwayatObstetri";
    var apiResult = await http.post(apiURL, body: {
      "id_pengguna": idpengguna,
      "k1": k1,
      "k2": k2,
      "k3": k3,
      "k4": k4,
      "k5": k5,
      "jika_ya": jikaya
    });
    var jsonObject = json.decode(apiResult.body);
    return RiwayatObstetri.createRiwayatObstetri(jsonObject);
  }
}
