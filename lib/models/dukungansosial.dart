part of 'models.dart';

class DukunganSosial {
  final String id;
  final String idpengguna;
  final String kekerasan;
  final String k1;
  final String k2;
  final String k3;
  final String k4;
  final String k5;
  final String k6;
  final String success;
  final String info;

  DukunganSosial(
      {this.id,
      this.idpengguna,
      this.kekerasan,
      this.k1,
      this.k2,
      this.k3,
      this.k4,
      this.k5,
      this.k6,
      this.success,
      this.info});

  factory DukunganSosial.createDukunganSosial(Map<String, dynamic> object) {
    return DukunganSosial(
        id: object['id'],
        idpengguna: object['id_pengguna'],
        kekerasan: object['kekerasan'],
        k1: object['k1'],
        k2: object['k2'],
        k3: object['k3'],
        k4: object['k4'],
        k5: object['k5'],
        k6: object['k6'],
        success: object['success'],
        info: object['info']);
  }

  static Future<DukunganSosial> getDukunganSosial(String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getDukunganSosial?id_pengguna=" + idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return DukunganSosial.createDukunganSosial(jsonObject);
  }

  static Future<DukunganSosial> apiSubmitDukunganSosial(
      String idpengguna,
      String kekerasan,
      String k1,
      String k2,
      String k3,
      String k4,
      String k5,
      String k6) async {
    String apiURL = "https://carebumil.com/api/apiSubmitDukunganSosial";
    var apiResult = await http.post(apiURL, body: {
      "id_pengguna": idpengguna,
      "kekerasan": kekerasan,
      "k1": k1,
      "k2": k2,
      "k3": k3,
      "k4": k4,
      "k5": k5,
      "k6": k6
    });
    var jsonObject = json.decode(apiResult.body);
    return DukunganSosial.createDukunganSosial(jsonObject);
  }
}
