part of 'models.dart';

class Kehamilan {
  final String id;
  final String idpengguna;
  final String hpl;
  final String hamilke;
  final String tanggalpertamahaid;
  final String hamildirencanakan;
  final String harapanjeniskelamin;
  final String kalauada;
  final String antidepresi;
  final String keluhan;
  final String jikaada;
  final String success;
  final String info;

  Kehamilan(
      {this.id,
      this.idpengguna,
      this.hpl,
      this.hamilke,
      this.tanggalpertamahaid,
      this.hamildirencanakan,
      this.harapanjeniskelamin,
      this.kalauada,
      this.antidepresi,
      this.keluhan,
      this.jikaada,
      this.success,
      this.info});

  factory Kehamilan.createKehamilan(Map<String, dynamic> object) {
    return Kehamilan(
        id: object['id'],
        idpengguna: object['id_pengguna'],
        hpl: object['hpl'],
        hamilke: object['hamil_ke'],
        tanggalpertamahaid: object['tanggal_pertama_haid'],
        hamildirencanakan: object['hamil_direncanakan'],
        harapanjeniskelamin: object['harapan_jenis_kelamin'],
        kalauada: object['kalau_ada'],
        antidepresi: object['antidepresi'],
        keluhan: object['keluhan'],
        jikaada: object['jika_ada'],
        success: object['success'],
        info: object['info']);
  }

  static Future<Kehamilan> getKehamilan(String idpengguna) async {
    String apiURL =
        "https://carebumil.com/api/getKehamilan?id_pengguna=" + idpengguna;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    return Kehamilan.createKehamilan(jsonObject);
  }

  static Future<Kehamilan> apiSubmitKehamilan(
      String idpengguna,
      String hamilke,
      String tanggalpertamahaid,
      String hamildirencanakan,
      String harapanjeniskelamin,
      String kalauada,
      String antidepresi,
      String keluhan,
      String jikaada) async {
    String apiURL = "https://carebumil.com/api/apiSubmitKehamilan";
    var apiResult = await http.post(apiURL, body: {
      "id_pengguna": idpengguna,
      "hamil_ke": hamilke,
      "tanggal_pertama_haid": tanggalpertamahaid,
      "hamil_direncanakan": hamildirencanakan,
      "harapan_jenis_kelamin": harapanjeniskelamin,
      "kalau_ada": kalauada,
      "antidepresi": antidepresi,
      "keluhan": keluhan,
      "jika_ada": jikaada
    });
    var jsonObject = json.decode(apiResult.body);
    return Kehamilan.createKehamilan(jsonObject);
  }
}
